/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	const getPerson = function() {
		let fullName = prompt('Enter your full name: ');
		let age = prompt('Enter your age: ');
		let address = prompt('Enter your address: ');

		console.log(`Hello, ${fullName}`);
		console.log(`You are ${age} years old.`);
		console.log(`You live in ${address}`);
	}

	getPerson();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	const getBands = () => {
		console.log('1. TWICE');
		console.log('2. Mayday Parade');
		console.log('3. All Time Low');
		console.log('4. Le Sserafim');
		console.log('5. Spongecola');
	}

	getBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	const getMovies = () => {
		console.log('1. Enemy At The Gates');
		console.log('Rotten Tomatoes Rating: 82%');
		console.log('2. Avatar');
		console.log('Rotten Tomatoes Rating: 82%');
		console.log('3. The Imitiation Game');
		console.log('Rotten Tomatoes Rating: 91%');
		console.log('4. Interstellar');
		console.log('Rotten Tomatoes Rating: 86%');
		console.log('5. Seven Sundays');
		console.log('Rotten Tomatoes Rating: NA');
	}

	getMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();



// console.log(friend1);
// console.log(friend2);
